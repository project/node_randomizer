CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Node Randomizer provides a block which when clicked redirects user to a random published node within the site.
This can be used to provide feature like "READ RANDOM ARTICLE" in the site.


REQUIREMENTS
------------

This module does not have any dependency other than drupal core.


INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

2. Once installed a new block "Node Randomizer" will be available. Place this block anywhere in the site.


CONFIGURATION
-------------

The configuration for this module is made in:
* Administration | Configuration | Node Randomizer | Node Randomizer

1. Global configuration includes choosing content types from which the randomized node will be selected for redirection.

2. On block level you can configure the block text, whether to use a material icon instead of simple text and the description.

3. This module creates a node/random route to generate the random node, as such you can even use this route in menu links or as 
   a normal link throughout the site which will achieve the same functionality as block.


MAINTAINERS
-----------

Current maintainers:
* Aalok Atre (AalokAtre) - https://www.drupal.org/u/aalokatre

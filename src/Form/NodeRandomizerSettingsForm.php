<?php

namespace Drupal\node_randomizer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides settings form to select content types.
 *
 * @package Drupal\node_randomizer\Form
 */
class NodeRandomizerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'node_randomizer.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_randomizer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_randomizer.settings');
    $content_types = node_type_get_names();
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $content_types,
      '#default_value' => $config->get('enabled_content_types'),
      '#title' => $this->t('Allowed Content Types'),
      '#description' => $this->t('If none are selected all are applicable.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('node_randomizer.settings')
      ->set('enabled_content_types', $form_state->getValue('content_types'))
      ->save();
  }

}

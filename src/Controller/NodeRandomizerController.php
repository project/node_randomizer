<?php

namespace Drupal\node_randomizer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Node randomizer controller.
 */
class NodeRandomizerController extends ControllerBase {

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Path\AliasManagerInterface $path_alias_manager
   *   The path alias manager.
   */
  public function __construct(AliasManagerInterface $path_alias_manager) {
    $this->pathAliasManager = $path_alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path_alias.manager')
    );
  }

  /**
   * Redirect the user to a random node or front page.
   */
  public function randomize() {
    $content_types = $this->config('node_randomizer.settings')->get('enabled_content_types');
    $langcode = $this->languageManager()->getCurrentLanguage()->getId();
    // Remove all content types which are not selected in config form.
    foreach ((array) $content_types as $content_type => $enabled) {
      if ($enabled == '0') {
        unset($content_types[$content_type]);
      }
    }
    // If no content type selected, all are allowed.
    if (empty($content_types)) {
      $node = $this->entityTypeManager()->getStorage('node')->loadByProperties([
        'status' => 1,
      ]);
    }
    // Else list from selected content types.
    else {
      $node = $this->entityTypeManager()->getStorage('node')->loadByProperties([
        'type' => $content_types,
        'status' => 1,
      ]);
    }
    // Check if node exists and redirect.
    if (!empty($node)) {
      foreach ($node as $key => $value) {
        $key_list[] = $key;
      }
      $nid = $key_list[array_rand($key_list)];
      $response = new RedirectResponse($this->pathAliasManager->getAliasByPath('/node/' . $nid, $langcode));
      $response->send();
    }
    // Redirect to front page if no node is found.
    // This may be due to no published nodes exist in selected content types
    // or no node exist in site.
    else {
      $frontpage = $this->config('system.site')->get('page.front');
      $response = new RedirectResponse($this->pathAliasManager->getAliasByPath($frontpage, $langcode));
      $response->send();
      $this->messenger()->addMessage($this->t('No content found. You are redirected to the homepage'));
    }
  }

}

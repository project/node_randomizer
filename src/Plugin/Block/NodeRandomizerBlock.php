<?php

namespace Drupal\node_randomizer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Node Randomizer' Block.
 *
 * @Block(
 *   id = "node_randomizer_block",
 *   admin_label = @Translation("Node Randomizer"),
 *   category = @Translation("Node Randomizer"),
 * )
 */
class NodeRandomizerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#theme' => 'node_randomizer_block',
      '#data' => [
        'text' => $config['randomizer_block_text'],
        'use_icon' => $config['use_icon'],
        'icon_code' => $config['icon_code'],
        'description' => $config['description'],
      ],
      '#attached' => [
        'library' => [
          'node_randomizer/baseline',
          'node_randomizer/outlined',
          'node_randomizer/two-tone',
          'node_randomizer/round',
          'node_randomizer/sharp',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Node Randomizer Settings'),
      '#description' => $this->t('Control how the block is displayed'),
      '#open' => TRUE,
    ];

    $form['advanced']['randomizer_block_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block Text'),
      '#default_value' => isset($config['randomizer_block_text']) ? $config['randomizer_block_text'] : $this->t('Read Random Content'),
      '#description' => $this->t('This text will be shown on the block'),
    ];

    $form['advanced']['use_icon'] = [
      '#type' => 'checkbox',
      '#title' => 'Use Icon',
      '#default_value' => isset($config['use_icon']) ? $config['use_icon'] : 1,
      '#description' => $this->t('Check this option to use material icon instead of text'),
    ];

    $form['advanced']['icon_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Material Icon Code'),
      '#default_value' => isset($config['icon_code']) ? $config['icon_code'] : '<span class="material-icons-outlined">shuffle_on</span>',
      '#description' => $this->t('You can find icon codes <a href="https://fonts.google.com/icons" target="_blank">here</a>.'),
    ];

    $form['advanced']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => isset($config['description']) ? $config['description'] : $this->t('Read Random Content'),
      '#description' => $this->t('Shown when hovering over the block.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['randomizer_block_text'] = $form_state->getValue([
      'advanced',
      'randomizer_block_text',
    ]);
    $this->configuration['use_icon'] = $form_state->getValue([
      'advanced',
      'use_icon',
    ]);
    $this->configuration['icon_code'] = $form_state->getValue([
      'advanced',
      'icon_code',
    ]);
    $this->configuration['description'] = $form_state->getValue([
      'advanced',
      'description',
    ]);
  }

}
